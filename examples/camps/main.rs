use anyhow::{bail, Error};
use chrono::Datelike;
use ini::Ini;
use monprojet_api::{camp, session};
use structopt::StructOpt;
use std::collections::HashMap;
use std::path::Path;

#[derive(StructOpt)]
struct Opt {
    #[structopt(short, long)]
    identifiant: Option<String>,
    #[structopt(short, long)]
    pass: Option<String>,
    #[structopt(short, long)]
    structure: String,
    #[structopt(short, long)]
    retry: bool,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let opts = Opt::from_args();
    let credentials = session::Credentials::new(opts.identifiant, opts.pass);
    if !credentials.is_valid() {
        bail!("Identifiant and Pass are mandatory!")
    }

    let mut session = session::Session::new();
    let res = session.login(credentials).await;
    if res.is_err() {
        println!("Unable to login:");
        res?;
    }
    let mut query = camp::MultiCriteresQuery::new();
    query = query.set_structure(opts.structure, true);
    let res = session.query_camps_multi(query).await;
    if res.is_err() {
        println!("Unable to query_camps_multi:");
    }
    let camps = res?;
    for camp in camps {
        if camp.statut != camp::CampStatut::EnCours {
            print!("Skipping {} (Status: {:#?})\n", camp.id, camp.statut);
            continue
        }

        let file = format!("{}.ini", camp.id);
        if opts.retry && Path::new(&file).exists() {
            print!("Skipping {} (File exists)\n", camp.id);
            continue
        }

        print!("Starting {}", camp.id);
        let mut conf = Ini::new();
        conf.with_section(Some("General"))
            .set("titre", camp.libelle)
            .set("type_camp", camp.type_camp.code.to_string())
            .set("date_debut", camp.date_debut.format("%d/%m/%Y").to_string())
            .set("date_fin", camp.date_fin.format("%d/%m/%Y").to_string());

        let mut staff_qualifs : HashMap<String,Vec<String>> = HashMap::new();
        let mut camp_lieu_principal : Option<camp::CampLieu> = camp.camp_lieu_principal;
        match camp.type_camp.code_groupe_camp {
            camp::CodeGroupeCamp::_8_17 => {
                let camp_general = session.query_camp_general (camp.id).await?;
                if let Some(c817) = camp_general.camp_817 {
                    conf.with_section(Some("General"))
                        .set("nombre_chefs_prevu", c817.prevision_nbre_animateurs.to_string())
                        .set("nombre_jeunes_prevu", c817.prevision_nbre_participants.to_string())
                        .set("nombre_jeunes_filles_prevu", c817.prevision_nbre_filles.to_string())
                        .set("nombre_jeunes_garcons_prevu", c817.prevision_nbre_garcons.to_string())
                        .set("nombre_jeunes_6_13_prevu", c817.prevision_nbre_613.to_string())
                        .set("nombre_jeunes_14_17_prevu", c817.prevision_nbre_1417.to_string());
                }

                camp_lieu_principal = camp_general.camp_lieu_principal;
                if let Some(camp_lieux) = camp_general.camp_lieux {
                    if camp_lieux.len() > 1 {
                        for (index, lieu) in camp_lieux.iter().enumerate() {
                            if let Some(ref_commune) = &lieu.tam_ref_commune {
                                let adresse = format!("{} {}",
                                    match &lieu.adresse_ligne1 {
                                        Some(a) => { a.as_str() },
                                        None => { "" },
                                    },
                                    match &lieu.adresse_ligne2 {
                                        Some(a) => { a.as_str() },
                                        None => { "" },
                                    }
                                );
                                conf.with_section(Some(format!("Etape:{}", index)))
                                    .set("date_debut", lieu.date_debut.unwrap_or(camp.date_debut).format("%d/%m/%Y").to_string())
                                    .set("date_fin", lieu.date_fin.unwrap_or(camp.date_fin).format("%d/%m/%Y").to_string())
                                    .set("libelle", lieu.libelle.as_ref().unwrap_or(&"".to_string()))
                                    .set("adresse", adresse.trim())
                                    .set("code_postal", &ref_commune.code_postal)
                                    .set("commune", &ref_commune.libelle)
                                    .set("code_insee", ref_commune.code_insee.as_ref().unwrap_or(&"".to_string()))
                                    .set("id_commune", ref_commune.id_tam_commune.as_ref().unwrap_or(&"".to_string()))
                                    .set("id_departement",  if let Some(tam_ref_departement) = &ref_commune.tam_ref_departement {tam_ref_departement.id_tam_departement.to_string()} else {"".to_string()});
                            }
                        }
                    }
                }

                if let Some(camp_staffs) = camp_general.camp_adherent_staffs_informations {
                    for staff in camp_staffs {
                      let mut qualifs = Vec::new();
                      for qualif in staff.adherent_qualifications {
                        qualifs.push(qualif.libelle.to_string() +
                            match qualif.est_titulaire {
                                true => " Titulaire",
                                false => " Stagiaire",
                            });
                      }

                      if !qualifs.is_empty() {
                        staff_qualifs.insert(staff.numero.clone(), qualifs);
                      }
                    }
                }
            },
            _ => {}
        };

        if let Some(camp_lieu_principal) = camp_lieu_principal {
            if let Some(ref_commune) = camp_lieu_principal.tam_ref_commune {
                let adresse = format!("{}{}",
                    match camp_lieu_principal.adresse_ligne1 {
                        Some(a) => { a },
                        None => { "".to_string() },
                    },
                    match camp_lieu_principal.adresse_ligne2 {
                        Some(a) => { format!(" {}", a) },
                        None => { "".to_string() },
                    }
                );
                conf.with_section(Some("Lieu"))
                    .set("date_debut", camp_lieu_principal.date_debut.unwrap_or(camp.date_debut).format("%d/%m/%Y").to_string())
                    .set("date_fin", camp_lieu_principal.date_fin.unwrap_or(camp.date_fin).format("%d/%m/%Y").to_string())
                    .set("libelle", camp_lieu_principal.libelle.as_ref().unwrap_or(&"".to_string()))
                    .set("adresse", adresse.trim())
                    .set("code_postal", ref_commune.code_postal)
                    .set("commune", ref_commune.libelle)
                    .set("code_insee", ref_commune.code_insee.as_ref().unwrap_or(&"".to_string()))
                    .set("id_commune", ref_commune.id_tam_commune.as_ref().unwrap_or(&"".to_string()))
                    .set("id_departement", if let Some(tam_ref_departement) = ref_commune.tam_ref_departement {tam_ref_departement.id_tam_departement.to_string()} else {"".to_string()});
            }
        }

        if let Some(camp_staffs) = camp.camp_adherent_staffs {
            for staff in camp_staffs {
                let header = format!("Chef:{}", staff.adherent.numero);
                conf.with_section(Some(&header))
                    .set("prenom", staff.adherent.prenom)
                    .set("nom", staff.adherent.nom)
                    .set("date_debut", staff.date_debut_presence.unwrap().format("%d/%m/%Y").to_string())
                    .set("date_fin", staff.date_fin_presence.unwrap().format("%d/%m/%Y").to_string());
                if staff.role_staff == "D" {
                    conf.with_section(Some("General")).set("directeur", &staff.adherent.numero);
                }
                if staff.responsabilite_as {
                    conf.with_section(Some("General")).set("assistant_sanitaire", &staff.adherent.numero);
                }

                if staff.validation_stage_pratique_bafa {
                    conf.with_section(Some(&header)).set("stage_pratique", "BAFA");
                } else if staff.validation_stage_pratique_bafd {
                    conf.with_section(Some(&header)).set("stage_pratique", "BAFD");
                }

                if let Some(qualifs) = staff_qualifs.get(&staff.adherent.numero) {
                    for (i, qualif) in qualifs.iter().enumerate() {
                        conf.with_section(Some(&header)).set(format!("qualification[{}]", i), qualif);
                    }
                }
            }
        }

        if let Some(camp_participants) = camp.camp_adherent_participants {
            let mut bucket_6_13 : u64 = 0;
            let mut bucket_14p : u64 = 0;
            for participant in camp_participants {
                let debut_presence = participant.date_debut_presence.unwrap_or(camp.date_debut);
                let date_naissance = participant.adherent.date_naissance.unwrap();
                let mut age = debut_presence.year() - date_naissance.year();
                if debut_presence.ordinal() < date_naissance.ordinal() {
                    age -= 1;
                }
                if age >= 6 && age <= 13 {
                    bucket_6_13 += 1;
                } else if age >= 14 {
                    bucket_14p += 1;
                }
            }
            let bucket_total = bucket_6_13 + bucket_14p;
            conf.with_section(Some("General"))
                .set("nombre_jeunes", bucket_total.to_string())
                .set("nombre_jeunes_6_13", bucket_6_13.to_string())
                .set("nombre_jeunes_14_plus", bucket_14p.to_string());
        }

        if let Some(avis_list) = camp.camp_avis {
            for avis in avis_list {
                conf.with_section(Some("Avis"))
                    .set(avis.type_avis, avis.statut);
            }
        }
        conf.with_section(Some("Avis"))
            .set("StatutDepartCamp", 
                 match camp.statut_depart_camp {
                     None => "none".to_string(),
                     Some(x) => x,
                 });

        let tam = camp.statut_declaration_tam.unwrap_or (camp::DeclarationTam::Aucune);
        conf.with_section(Some("Avis"))
            .set("DeclarationTam", format!("{:#?}", tam));

        conf.write_to_file(file).unwrap();
        print!(" [DONE]\n");
    }
    Ok(())
}
