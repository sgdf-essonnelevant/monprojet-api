use anyhow::{bail, Error};
use ini::Ini;
use monprojet_api::{projet_annee, session};
use structopt::StructOpt;
use std::collections::HashMap;

#[derive(StructOpt)]
struct Opt {
    #[structopt(short, long)]
    identifiant: Option<String>,
    #[structopt(short, long)]
    pass: Option<String>,
    #[structopt(short, long)]
    structure: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let opts = Opt::from_args();
    let credentials = session::Credentials::new(opts.identifiant, opts.pass);
    if !credentials.is_valid() {
        bail!("Identifiant and Pass are mandatory!")
    }

    let mut session = session::Session::new();
    session.login(credentials).await?;
    let mut query = projet_annee::MultiCriteresQuery::new();
    query = query.set_structure(opts.structure, true);
    let projets_annee = session.query_projet_annee(query).await?;
    for projet_annee in projets_annee {
        if projet_annee.statut_projet_declaration != projet_annee::ProjetAnneeStatut::EnCours {
            print!("Skipping {} (Status: {:#?})\n", projet_annee.id, projet_annee.statut_projet_declaration);
            continue
        }

        print!("Starting {}", projet_annee.id);
        let mut conf = Ini::new();
        conf.with_section(Some("General"))
            .set("titre", projet_annee.libelle);

        let projet_annee_staff = session.query_projet_annee_staff (projet_annee.id).await?;

        let mut staff_fonctions = HashMap::new();
        let mut staff_qualifs : HashMap<String,Vec<String>> = HashMap::new();
        let mut staff_formations : HashMap<String,Vec<String>> = HashMap::new();
        let mut staff_diplomes : HashMap<String,Vec<String>> = HashMap::new();
        if let Some(projet_annee_staffs) = projet_annee_staff.projet_annee_adherent_staffs_informations {
            for staff in projet_annee_staffs {
              staff_fonctions.insert(staff.numero.clone(), staff.fonction.to_string());
              let mut qualifs = Vec::new();
              for qualif in staff.adherent_qualifications {
                qualifs.push(qualif.libelle.to_string() +
                    match qualif.est_titulaire {
                        true => " Titulaire",
                        false => " Stagiaire",
                    });
              }
              if !qualifs.is_empty() {
                staff_qualifs.insert(staff.numero.clone(), qualifs);
              }
              let mut formations = Vec::new();
              for formation in staff.adherent_formations {
                if formation.role == "Stagiaire" {
                  formations.push(formation.stage.to_string());
                }
              }
              if !formations.is_empty() {
                staff_formations.insert(staff.numero.clone(), formations);
              }
              let mut diplomes = Vec::new();
              for diplome in staff.adherent_diplomes {
                diplomes.push(diplome.libelle.to_string());
              }
              if !diplomes.is_empty() {
                staff_diplomes.insert(staff.numero.clone(), diplomes);
              }
            }
        }

        if let Some(projet_annee_adherent_staffs) = projet_annee_staff.projet_annee_adherent_staffs {
            for staff in projet_annee_adherent_staffs {
                let header = format!("Chef:{}", staff.adherent.numero);
                conf.with_section(Some(&header))
                    .set("prenom", staff.adherent.prenom)
                    .set("nom", staff.adherent.nom);
                if staff.role_staff == "D" {
                    conf.with_section(Some("General")).set("directeur", &staff.adherent.numero);
                    conf.with_section(Some(&header)).set("role", "directeur");
                } else if staff.role_staff == "DA" {
                    conf.with_section(Some("General")).set("directeur_adjoint", &staff.adherent.numero);
                    conf.with_section(Some(&header)).set("role", "directeur_adjoint");
                } else if staff.role_staff == "C" {
                    conf.with_section(Some(&header)).set("role", "animateur");
                } else if staff.role_staff == "A" {
                    conf.with_section(Some(&header)).set("role", "autre");
                }

                if staff.validation_stage_pratique_bafa {
                    conf.with_section(Some(&header)).set("stage_pratique", "BAFA");
                } else if staff.validation_stage_pratique_bafd {
                    conf.with_section(Some(&header)).set("stage_pratique", "BAFD");
                }

                if let Some(fonction) = staff_fonctions.get(&staff.adherent.numero) {
                    conf.with_section(Some(&header)).set("fonction", fonction);
                }
                if let Some(qualifs) = staff_qualifs.get(&staff.adherent.numero) {
                    for (i, qualif) in qualifs.iter().enumerate() {
                        conf.with_section(Some(&header)).set(format!("qualification[{}]", i), qualif);
                    }
                }
                if let Some(formations) = staff_formations.get(&staff.adherent.numero) {
                    for (i, formation) in formations.iter().enumerate() {
                        conf.with_section(Some(&header)).set(format!("formation[{}]", i), formation);
                    }
                }
                if let Some(diplomes) = staff_diplomes.get(&staff.adherent.numero) {
                    for (i, diplome) in diplomes.iter().enumerate() {
                        conf.with_section(Some(&header)).set(format!("diplome[{}]", i), diplome);
                    }
                }
            }
        }

        conf.write_to_file(format!("{}.ini", projet_annee.id)).unwrap();
        print!(" [DONE]\n");
    }
    Ok(())
}
