use anyhow::{bail, Error};
use monprojet_api::{camp, session};
use structopt::StructOpt;
use std::io::Write;
use std::path::Path;
use std::fs::File;

#[derive(StructOpt)]
struct Opt {
    #[structopt(short, long)]
    identifiant: Option<String>,
    #[structopt(short, long)]
    pass: Option<String>,
    #[structopt(short, long)]
    structure: String,
    #[structopt(short = "a", long)]
    regenerate: bool,
    #[structopt(short, long)]
    retry: bool,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let opts = Opt::from_args();
    let credentials = session::Credentials::new(opts.identifiant, opts.pass);
    if !credentials.is_valid() {
        bail!("Identifiant and Pass are mandatory!")
    }

    let mut session = session::Session::new();
    let res = session.login(credentials).await;
    if res.is_err() {
        println!("Unable to login:");
        res?;
    }
    let mut query = camp::MultiCriteresQuery::new();
    query = query.set_structure(opts.structure, true).add_status (camp::StatusCamp::EnCours);
    let res = session.query_camps_multi(query).await;
    if res.is_err() {
        println!("Unable to query camps multi:");
    }

    let camps = res?;
    for camp in camps {
        if camp.statut != camp::CampStatut::EnCours {
            println!("Skipping {} (Status: {:#?})", camp.id, camp.statut);
            continue
        }

        let path_string = format!("{}.zip", camp.id);
        let path = Path::new(&path_string);
        if opts.retry && path.exists() {
            print!("Skipping {} (File exists)\n", camp.id);
            continue
        }

        print!("Starting {}", camp.id);
        if opts.regenerate {
            let res = session.generate_dossier_camp (camp.id).await;
            if res.is_err() {
                print!(" Unable to regenerate, ignoring this error!");
            }
        }

        let res = session.query_camp_fichiers (camp.id).await;
        if res.is_err() {
            println!("Unable to query_camp_fichiers:");
        }
        let camp_fichiers = res?;
        if camp_fichiers.is_empty() {
            print!(" - Empty! [DONE]\n");
            continue;
        } else {
            print!(" - {} file(s)", camp_fichiers.len());
        }

        let file = match File::create(&path) {
            Err(why) => panic!("couldn't create {}: {}", path.display(), why),
            Ok(file) => file,
        };

        let mut zip = zip::ZipWriter::new(file);
        let options = zip::write::FileOptions::default().compression_method(zip::CompressionMethod::Stored);
        for camp_fichier in camp_fichiers {
            zip.start_file(camp_fichier.nom, options)?;
            let file_content = session.download_file(camp_fichier.id).await?;
            zip.write_all(file_content.as_ref())?;
        }

        zip.finish()?;

        print!(" [DONE]\n");
    }
    Ok(())
}
