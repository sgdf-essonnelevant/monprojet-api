use anyhow::{bail, Error};
use monprojet_api::{adherents, session};
use structopt::StructOpt;
use ical::generator::*;
use ical::{ical_property, ical_param};
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::collections::HashMap;
use configparser::ini::Ini;
use std::str::FromStr;
extern crate case;
use crate::case::CaseExt;


#[derive(StructOpt)]
struct Opt {
    #[structopt(short, long)]
    identifiant: Option<String>,
    #[structopt(short, long)]
    pass: Option<String>,
    #[structopt(short, long)]
    structure: String,
}

fn fonction_beautiful(libelle: &String, genre: u16) -> String {
    let libelle_parts : Vec<&str> = libelle.split("/").collect();
    let libelle = libelle_parts.get(genre as usize);
    libelle.unwrap_or(libelle_parts.get(0).unwrap()).trim().to_lowercase().to_camel()
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let opts = Opt::from_args();
    let credentials = session::Credentials::new(opts.identifiant, opts.pass);
    if !credentials.is_valid() {
        bail!("Identifiant and Pass are mandatory!")
    }

    let mut config = Ini::new();
    let mut structures_map : HashMap<String, Option<String>> = HashMap::new();
    if let Some(mut config_dir) = dirs::config_dir() {
        config_dir.push("monprojet.ini");
        if let Ok(map) = config.load(config_dir) {
            if let Some(structure) = map.get("structures") {
                structures_map = structure.clone();
            }
        }
    }

    let mut session = session::Session::new();
    session.login(credentials).await?;
    let mut query = adherents::AdherentsQuery::new(opts.structure.clone());
    let adherents = session.query_adherents(query).await?;

    let filename = format!("{}.vcard", opts.structure);
    let path = Path::new(&filename);
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", path.display(), why),
        Ok(file) => file,
    };

    for adherent in adherents {
        let mut vcard = IcalVcardBuilder::version("4.0")
            .names(
                Some(adherent.nom.to_lowercase().to_camel()),
                Some(adherent.prenom.to_lowercase().to_camel()),
                None,
                None,
                None,
            )
            .generate_fn();
        if let Some(email) = adherent.email {
            vcard = vcard.set(ical_property!("EMAIL", email));
        }
        if let Some(telephone_portable) = adherent.telephone_portable {
            for tel in telephone_portable.split("/") {
                let mut tel = tel.trim().to_string();
                if tel.starts_with("0") {
                    tel.replace_range(..1, "+33");
                }
                vcard = vcard.set(ical_property!(
                    "TEL",
                    tel,
                    ical_param!("TYPE", "home")
                ));
            }
        }
        if let Some(date_naissance) = adherent.date_naissance {
            vcard = vcard.set(ical_property!("BDAY", format!("{}", date_naissance.format("%Y%m%d"))));
        }
        let mut categories: Vec<String> = Vec::new();
        for structure_adherent in adherent.structure_adherents {
            if structure_adherent.rattachement_principal {
                vcard = vcard.set(ical_property!("ROLE", fonction_beautiful (&structure_adherent.fonction.libelle, adherent.civilite.clone().map_or(1, |s| s.genre))));
            }
            let structure_code = i32::from_str(&structure_adherent.structure.code).unwrap();
            let parent_struct = structure_code - structure_code%100;
            let territoire_struct = structure_code - structure_code%1000;
            let structure_name = match structures_map.get(&parent_struct.to_string ()) {
                Some(t) => {t.clone().unwrap()},
                None => {structure_adherent.structure.libelle.to_lowercase().to_camel()}
            };
            categories.push(structure_name);
            if parent_struct != territoire_struct {
                match (structure_code%100 - structure_code%10)/10 {
                    1 => {categories.push("Oranges".to_string())},
                    2 => {categories.push("Bleus".to_string())},
                    3 => {categories.push("Rouges".to_string())},
                    4 => {categories.push("Compagnons".to_string())},
                    7 => {categories.push("Farfadets".to_string())},
                    8 => {categories.push("IMPEESA".to_string())},
                    _ => {},
                }
            }
        }
        categories.dedup();
        vcard = vcard.set(ical_property!("CATEGORIES", categories.join(",")));
        file.write_all(vcard.build().generate().as_bytes())?;
    }

    Ok(())
}
