use serde::Deserialize;
use thiserror::Error;
use std::collections::HashMap;
use bytes::Bytes;
use scraper::{Html, Selector};
use crate::camp::{Avis, Camp, CampAdherentParticipant, CampDetail, CampLieu, CampQuery, CampFichier, DeclarationTam};
use crate::camp;
use crate::projet_annee::ProjetAnnee;
use crate::projet_annee;
use crate::adherents;
use crate::adherents::AdherentsQuery;
extern crate dirs;
use configparser::ini::Ini;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Functionality {
    code_structure: String,
    structure_level: i64,
    libelle_structure: String,
    structure_geo_mask: String,
    code_fonction: String,
    libelle_fonction: String,
    is_rattachement_principal: bool,
    //"droitsOrganisateur": [],
    //"droitsParticipant": []
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UserData {
    numero: String,
    full_name: String,
    is_national: bool,
    #[serde(rename(deserialize = "isADMIN"))]
    is_admin: bool,
    #[serde(rename(deserialize = "isCOMMAR"))]
    is_commar: bool,
    #[serde(rename(deserialize = "isCMPP"))]
    is_cmpp: bool,
    #[serde(rename(deserialize = "isDT"))]
    is_dt: bool,
    #[serde(rename(deserialize = "donnerAvisAP"))]
    donner_avis_ap: bool,
    #[serde(rename(deserialize = "donnerAvisRG"))]
    donner_avis_rg: bool,
    autoriser_depart: bool,
    #[serde(rename(deserialize = "teledeclarerDCCS"))]
    teledeclarer_ddcs: bool,
    #[serde(rename(deserialize = "modifierInterlocAP"))]
    modifier_interloc_ap: bool,
    acces_projet_annee: bool,
    creer_projet_annee: bool,
    modifier_projet_annee: bool,
    declarer_projet_annee: bool,
    reset_declaration_projet_annee: bool,
    administrer_pays: bool,
    selected_functionality: Functionality,
    functionalities: Vec<Functionality>,
    modifier_checklist_module: bool,
}

#[derive(Debug)]
pub struct Credentials {
    id: Option<String>,
    password: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Login {
    token: String,
    user_data: UserData,
}

#[derive(Debug)]
pub struct Session {
    client: reqwest::Client,
    login: Option<Login>,
}

#[derive(Error, Debug)]
pub enum SessionError {
    #[error("Request error for {:?}: `{0}`", .0.url())]
    ReqwestError(#[from] reqwest::Error),
    #[error("Invalid credentials")]
    InvalidCredentials,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampPager {
    camps: Vec<Camp>,
    camps_total_count: u32,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampDetailPager {
    camps: Vec<CampDetail>,
    camps_total_count: u32,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProjetAnneePager {
    projets_annee: Vec<ProjetAnnee>,
    projets_annee_total_count: u32,
}

impl Credentials {
    pub fn new(manual_id: Option<String>, manual_password: Option<String>) -> Credentials {
        let mut config = Ini::new();
        let mut id : Option<String> = None;
        let mut password : Option<String> = None;
        if let Some(mut config_dir) = dirs::config_dir() {
            config_dir.push("monprojet.ini");
            let _ = config.load(config_dir);
            id = config.get("General", "Id");
            password = config.get("General", "Password");
        }

        if let Some(_) = manual_id {
            id = manual_id;
        }

        if let Some(_) = manual_password {
            password = manual_password;
        }

        Credentials {
            id: id,
            password: password,
        }
    }

    pub fn is_valid(&self) -> bool {
        match self.id {
            Some(_) => {}
            None => { return false; }
        }

        match self.password {
            Some(_) => {}
            None => { return false; }
        }
        return true;
    }
}

impl Session {
    pub fn new() -> Session {
        let client = reqwest::Client::builder().cookie_store(true).build().unwrap();
        Session {
            client,
            login: None,
        }
    }

    pub async fn login(&mut self, credentials: Credentials) -> Result<(), SessionError> {
        if !credentials.is_valid() {
            return Err(SessionError::InvalidCredentials);
        }

        let req = self.client.get("https://monprojet.sgdf.fr/api/login")
            .send()
            .await?
            .text()
            .await?;
        let doc = Html::parse_document(&req);
        let form_selector = Selector::parse("form").unwrap();
        let form = doc.select(&form_selector).next().unwrap();
        let auth_url = form.attr("action").unwrap();

        let mut map = HashMap::new();
        map.insert("username", credentials.id.clone().unwrap());
        map.insert("password", credentials.password.clone().unwrap());
        map.insert("credentialId", "".to_string());
        let resp = self.client.post(auth_url)
            .form(&map)
            .send()
            .await?;

        let mut login_loc = resp.url().clone();

        login_loc.set_path("api/login/check");

        let req = self.client.get(login_loc);
        let login = req.send().await?.json::<Login>().await?;
        self.login = Some(login);
        Ok(())
    }

    pub async fn query_adherents(&self, query: AdherentsQuery) -> Result<Vec<adherents::AdherentInfo>, SessionError> {
        let req = self.client.get(format!("https://monprojet.sgdf.fr/api/{}", query.create()))
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr");
        let adherents = req.send().await?.json::<Vec<adherents::AdherentInfo>>().await?;
        Ok(adherents)
    }

    pub async fn query_camps(&self, query: CampQuery) -> Result<Vec<Camp>, SessionError> {
        let mut camps = Vec::new();
        let mut page_num : u32 = 1;
        loop {
            let query_clone = query.clone().set_page(page_num);
            let req = self.client.get(format!("https://monprojet.sgdf.fr/api/{}", query_clone.create()))
                .bearer_auth(self.login.as_ref().unwrap().token.as_str())
                .header("Origin", "https://monprojet.sgdf.fr");
            let mut page_camps = req.send().await?.json::<CampPager>().await?;
            camps.append(&mut page_camps.camps);
            page_num = page_num + 1;
            if camps.len() as u32 == page_camps.camps_total_count {
                break;
            }
        }
        Ok(camps)
    }

    pub async fn query_camps_multi(&self, query: camp::MultiCriteresQuery) -> Result<Vec<CampDetail>, SessionError> {
        let mut camps = Vec::new();
        let mut page_num : u32 = 1;
        loop {
            let query_clone = query.clone().set_page(page_num);
            let query_str = query_clone.create();
            let req = self.client.get(format!("https://monprojet.sgdf.fr/api/camps/{}", query_str))
                .bearer_auth(self.login.as_ref().unwrap().token.as_str())
                .header("Origin", "https://monprojet.sgdf.fr");
            let resp = req.send().await?;
            let mut page_camps = resp.json::<CampDetailPager>().await?;
            camps.append(&mut page_camps.camps);
            page_num = page_num + 1;
            if camps.len() as u32 == page_camps.camps_total_count {
                break;
            }
        }
        Ok(camps)
    }

    pub async fn query_camp_general(&self, id: u64) -> Result<CampDetail, SessionError> {
        let req = self.client.get(format!("https://monprojet.sgdf.fr/api/camps/{}?module=INFO_GENERALE", id))
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr");
        let camp = req.send().await?.json::<CampDetail>().await?;
        Ok(camp)
    }

    pub async fn query_camp_lieu(&self, id: u64) -> Result<(Option<CampLieu>,Option<Vec<CampLieu>>), SessionError> {
        let req = self.client.get(format!("https://monprojet.sgdf.fr/api/camps/{}?module=LIEUX", id))
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr");
        let resp = req.send().await?;
        let camp = resp.json::<CampDetail>().await?;
        Ok((camp.camp_lieu_principal,camp.camp_lieux))
    }

    pub async fn query_camp_staff(&self, id: u64) -> Result<CampDetail, SessionError> {
        let req = self.client.get(format!("https://monprojet.sgdf.fr/api/camps/{}?module=STAFF", id))
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr");
        let camp = req.send().await?.json::<CampDetail>().await?;
        Ok(camp)
    }

    pub async fn query_camp_adherents_participants(&self, id: u64) -> Result<Option<Vec<CampAdherentParticipant>>, SessionError> {
        let req = self.client.get(format!("https://monprojet.sgdf.fr/api/camps/{}?module=PARTICIPANT", id))
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr");
        let camp = req.send().await?.json::<CampDetail>().await?;
        Ok(camp.camp_adherent_participants)
    }

    pub async fn query_camp_avis(&self, id: u64) -> Result<(Option<Vec<Avis>>,Option<String>), SessionError> {
        let req = self.client.get(format!("https://monprojet.sgdf.fr/api/camps/{}?module=SUIVI_SGDF", id))
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr");
        let camp = req.send().await?.json::<CampDetail>().await?;
        Ok((camp.camp_avis,camp.statut_depart_camp))
    }

    pub async fn query_tam(&self, id: u64) -> Result<DeclarationTam, SessionError> {
        let req = self.client.get(format!("https://monprojet.sgdf.fr/api/camps/{}?module=SUIVI_DDCS", id))
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr");
        let camp = req.send().await?.json::<CampDetail>().await?;
        Ok(camp.statut_declaration_tam.unwrap_or (DeclarationTam::Aucune))
    }

    pub async fn query_projet_annee(&self, query: projet_annee::MultiCriteresQuery) -> Result<Vec<ProjetAnnee>, SessionError> {
        let mut projets_annee = Vec::new();
        let mut page_num : u32 = 1;
        loop {
            let query_clone = query.clone().set_page(page_num);
            let req = self.client.get(format!("https://monprojet.sgdf.fr/api/projets-annee/{}", query_clone.create()))
                .bearer_auth(self.login.as_ref().unwrap().token.as_str())
                .header("Origin", "https://monprojet.sgdf.fr");
            let resp = req.send().await?;
            let mut page_projets_annee = resp.json::<ProjetAnneePager>().await?;
            projets_annee.append(&mut page_projets_annee.projets_annee);
            page_num = page_num + 1;
            if projets_annee.len() as u32 == page_projets_annee.projets_annee_total_count {
                break;
            }
        }
        Ok(projets_annee)
    }

    pub async fn query_projet_annee_staff(&self, id: u64) -> Result<ProjetAnnee, SessionError> {
        let req = self.client.get(format!("https://monprojet.sgdf.fr/api/projets-annee/{}?module=STAFF", id))
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr");
        let camp = req.send().await?.json::<ProjetAnnee>().await?;
        Ok(camp)
    }

    pub async fn query_camp_fichiers(&self, id: u64) -> Result<Vec<CampFichier>, SessionError> {
        let req = self.client.get(format!("https://monprojet.sgdf.fr/api/ddc-fichiers?idDdc={}&typeDdc=CAMP", id))
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr");
        let camp = req.send().await?.json::<Vec<CampFichier>>().await?;
        Ok(camp)
    }

    pub async fn generate_dossier_camp(&self, id: u64) -> Result<CampFichier, SessionError> {
        let form = reqwest::multipart::Form::new()
            .text("idCamp", id.to_string())
            .text("categorie", "DOSSIER_CAMP");
        let req = self.client.post("https://monprojet.sgdf.fr/api/ddc-fichiers/generate-dossier-camp")
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr")
            .multipart(form);
        let ddc = req.send().await?.json::<CampFichier>().await?;
        Ok(ddc)
    }

    pub async fn download_file(&self, id: u64) -> Result<Bytes, SessionError> {
        let req = self.client.get(format!("https://monprojet.sgdf.fr/api/ddc-fichiers/download/{}?typeDdc=CAMP", id))
            .bearer_auth(self.login.as_ref().unwrap().token.as_str())
            .header("Origin", "https://monprojet.sgdf.fr");
        let file_content = req.send().await?.bytes().await?;
        Ok(file_content)
    }


}

impl Default for Session {
    fn default() -> Self {
        Self::new()
    }
}
