use std::fmt;
use chrono::{ DateTime, Utc };
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentCivilite {
    pub id: u64,
    pub libelle: String,
    pub genre: u16,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StructureDesc {
    pub code: String,
    pub libelle: String,
    pub code_departement: String,
    pub actif: bool,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FonctionDesc {
    pub code: String,
    pub libelle: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StructureAdherent {
    pub id: u64,
    pub structure: StructureDesc,
    pub fonction: FonctionDesc,
    pub rattachement_principal: bool,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentInfo {
    pub numero: String,
    pub prenom: String,
    pub nom: String,
    pub nom_naissance: String,
    pub civilite: Option<AdherentCivilite>,
    pub date_naissance: Option<DateTime<Utc>>,
    pub telephone_portable: Option<String>,
    pub email: Option<String>,
    pub structure_adherents: Vec<StructureAdherent>,
}

#[derive(Debug, Default, Clone)]
pub struct AdherentsQuery {
    structure: String,
    adherent_type: AdherentType,
    full_access: bool,
    structure_dependante: bool,
}

#[derive(Debug, Clone)]
enum AdherentType {
    Staff,
}

impl fmt::Display for AdherentType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            AdherentType::Staff => write!(f, "STAFF"),
        }
    }
}

impl Default for AdherentType {
    fn default() -> Self { AdherentType::Staff }
}

impl AdherentsQuery {
    pub fn new(structure: String) -> AdherentsQuery {
        AdherentsQuery {
            structure: structure,
            adherent_type: AdherentType::Staff,
            full_access: true,
            structure_dependante: true,
        }
    }

    pub fn create(self) -> String {
        String::from(format!("adherents?codeStructure={}&campAdherentType={}&fullAccess={}&structureDependante={}", self.structure, self.adherent_type, self.full_access, self.structure_dependante))
    }
}
