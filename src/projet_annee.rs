use chrono::{ Datelike, DateTime, Utc, NaiveDateTime };
use serde::Deserialize;
use serde_repr::*;

#[derive(Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u8)]
pub enum ProjetAnneeStatut {
    EnCours = 1,
    Archive = 2
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentCivilite {
    pub id: u64,
    pub libelle: String,
    pub genre: u16,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentInfo {
    pub numero: String,
    pub prenom: String,
    pub nom: String,
    pub civilite: Option<AdherentCivilite>,
    pub date_naissance: Option<DateTime<Utc>>,
    pub telephone_pere: Option<String>,
    pub telephone_mere: Option<String>,
    pub telephone_portable: Option<String>,
    pub email: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProjetAnneeAdherentStaff {
    pub id: u64,
    pub adherent: AdherentInfo,
    pub role_staff: String,
    pub validation_stage_pratique_bafa: bool,
    pub validation_stage_pratique_bafd: bool,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentQualification {
    pub id: u64,
    pub est_titulaire: bool,
    #[serde(rename(deserialize = "type"))]
    pub libelle: String,
    pub date_fin: Option<NaiveDateTime>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentFormation {
    pub id: u64,
    pub libelle: Option<String>,
    #[serde(rename(deserialize = "type"))]
    pub stage: String,
    pub date_fin: Option<NaiveDateTime>,
    pub role: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentDiplome {
    pub id: u64,
    #[serde(rename(deserialize = "type"))]
    pub libelle: String,
    pub date_obtention: Option<NaiveDateTime>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentStaffInfo {
    pub numero: String,
    pub prenom: String,
    pub nom: String,
    pub nom_naissance: String,
    pub id_civilite: u64,
    pub statut: String,
    pub fonction: String,
    pub lieu_de_naissance: String,
    pub code_insee: String,
    pub date_de_naissance: NaiveDateTime,
    pub telephone_bureau: Option<String>,
    pub telephone_domicile: Option<String>,
    pub telephone_portable1: Option<String>,
    pub telephone_portable2: Option<String>,
    pub email: Option<String>,
    pub adherent_qualifications: Vec<AdherentQualification>,
    pub adherent_formations: Vec<AdherentFormation>,
    pub adherent_diplomes: Vec<AdherentDiplome>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProjetAnnee {
    pub id: u64,
    pub libelle: String,
    pub statut_projet_declaration: ProjetAnneeStatut,
    pub projet_annee_adherent_staffs: Option<Vec<ProjetAnneeAdherentStaff>>,
    pub projet_annee_adherent_staffs_informations: Option<Vec<AdherentStaffInfo>>,
}

#[derive(Debug, Clone)]
pub struct MultiCriteresQuery {
    tam_exercice: u32,
    code_structure: Option<String>,
    structures_dependantes: bool,
    page: u32,
}

impl MultiCriteresQuery {
    pub fn new() -> MultiCriteresQuery {
        let today = chrono::Utc::now().date_naive();

        MultiCriteresQuery {
            tam_exercice: if today.month() >= 9 { today.year() - 2004 } else { today.year() - 2003} as u32,
            code_structure: Option::None,
            structures_dependantes: true,
            page: 1
        }
    }

    pub fn create(self) -> String {
        String::from(format!("multi-criteres?idsTamRefExercices={}&codeStructure={}&chercherStructuresDependates={}&selectedPage={}",
            self.tam_exercice,
            self.code_structure.unwrap(),
            if self.structures_dependantes == true { "1" } else { "0" },
            self.page)
        )
    }

    pub fn set_page(mut self, page: u32) -> MultiCriteresQuery {
        self.page = page;
        self
    }

    pub fn set_structure(mut self, code_structure: String, sous_structures: bool) -> MultiCriteresQuery {
        self.code_structure = Option::Some(code_structure);
        self.structures_dependantes = sous_structures;
        self
    }
}
