use chrono::{NaiveDate, NaiveDateTime, Datelike, DateTime, Utc};
use serde::Deserialize;
use serde_repr::*;
use std::convert::Infallible;
use std::str::FromStr;
use std::fmt;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TypeModule {
    pub code: String,
    pub libelle: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ModuleDesc {
    pub code: String,
    pub rattachement_multiple: bool,
    pub libelle: Option<String>,
    pub type_moss: Option<String>,
    pub ordre_affichage: Option<u64>,
    pub type_module: Option<TypeModule>,
    pub question: Option<String>,
    pub description: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Module {
    pub id: u64,
    pub module: ModuleDesc,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TypeCamp {
    pub code: String,
    pub code_groupe_camp: CodeGroupeCamp,
    pub modules: Vec<Module>
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct HistoCreation {
    pub id: u64,
    pub date_heure_modification: DateTime<Utc>,
    pub adherent: AdherentInfo,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentCivilite {
    pub id: u64,
    pub libelle: String,
    pub genre: u16,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentInfo {
    pub numero: String,
    pub prenom: String,
    pub nom: String,
    pub civilite: Option<AdherentCivilite>,
    pub date_naissance: Option<DateTime<Utc>>,
    pub telephone_pere: Option<String>,
    pub telephone_mere: Option<String>,
    pub telephone_portable: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentQualification {
    pub id: u64,
    pub est_titulaire: bool,
    #[serde(rename(deserialize = "type"))]
    pub libelle: String,
    pub date_fin: Option<NaiveDateTime>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AdherentStaffInfo {
    pub numero: String,
    pub prenom: String,
    pub nom: String,
    pub nom_naissance: String,
    pub id_civilite: u64,
    pub statut: String,
    pub fonction: String,
    pub lieu_de_naissance: String,
    pub code_insee: String,
    pub date_de_naissance: NaiveDateTime,
    pub telephone_bureau: Option<String>,
    pub telephone_domicile: Option<String>,
    pub telephone_portable1: Option<String>,
    pub telephone_portable2: Option<String>,
    pub email: Option<String>,
    pub adherent_qualifications: Vec<AdherentQualification>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Adherent {
    pub id: u64,
    pub adherent: AdherentInfo,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Avis {
    pub id: u64,
    pub type_avis: String,
    pub date_heure_dernier_avis: Option<DateTime<Utc>>,
    pub statut: String,
}

#[derive(Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u8)]
pub enum CampStatut {
    EnCours = 1,
    Annule = 2,
    Archive = 3
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Camp {
    pub id: u64,
    pub libelle: String,
    pub statut: CampStatut,
    pub code_groupe_camp: CodeGroupeCamp,
    pub date_debut: DateTime<Utc>,
    pub date_fin: DateTime<Utc>,
    pub type_camp: TypeCamp,
    pub histo_creation: HistoCreation,
    pub camp_adherent_participants: Vec<Adherent>,
    pub camp_adherent_staffs: Vec<Adherent>
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Camp817 {
    pub prevision_nbre_animateurs: u64,
    pub prevision_nbre_equipes: u64,
    pub prevision_nbre_participants: u64,
    pub prevision_nbre_filles: u64,
    pub prevision_nbre_garcons: u64,
    pub prevision_nbre_613: u64,
    pub prevision_nbre_1417: u64,
    pub age_mini: u64,
    pub age_maxi: u64,
    pub projet_peda_bilan_annee: Option<String>,
    pub projet_peda_ambitions: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampAdherentStaff {
    pub id: u64,
    pub date_debut_presence: Option<DateTime<Utc>>,
    pub date_fin_presence: Option<DateTime<Utc>>,
    pub adherent: AdherentInfo,
    pub role_staff: String,
    pub validation_stage_pratique_bafa: bool,
    pub validation_stage_pratique_bafd: bool,
    pub responsabilite_intendant: bool,
    pub responsabilite_tresorier: bool,
    #[serde(rename(deserialize = "responsabiliteAS"))]
    pub responsabilite_as: bool,
    pub responsabilite_materiel: bool,
    pub responsabilite_autre: bool,
    pub responsabilite_autre_detail: Option<String>,
    pub commentaire_adherent_staff: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TamRefDepartement {
    pub id_tam_departement: u64,
    pub libelle: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TamRefCommune {
    pub id: u64,
    pub libelle: String,
    #[serde(rename(deserialize = "codePostale"))]
    pub code_postal: String,
    pub code_insee: Option<String>,
    pub id_tam_commune: Option<String>,
    pub tam_ref_departement: Option<TamRefDepartement>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TamRefPays {
    pub id: u64,
    pub libelle: String,
    pub id_tam_pays: Option<u64>,
    pub code_continent: Option<u64>,
    pub pays_ouvert: Option<bool>,
    pub email_contact: Option<String>,
    pub date_heure_modification: Option<DateTime<Utc>>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GeoJSon {
    pub latitude: String,
    pub longitude: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampLieu {
    pub id: u64,
    pub date_debut: Option<DateTime<Utc>>,
    pub date_fin: Option<DateTime<Utc>>,
    pub libelle: Option<String>,
    pub adresse_ligne1: Option<String>,
    pub adresse_ligne2: Option<String>,
    pub code_postal: Option<String>,
    pub ville: Option<String>,
    pub pays: Option<String>,
    pub geo_json: Option<GeoJSon>,
    pub telephone: Option<String>,
    pub feux_autorise: Option<bool>,
    pub en_france: Option<bool>,
    pub commentaire: Option<String>,
    pub nbre_km: Option<u64>,
    pub moyen_transport: Option<String>,
    pub tam_ref_commune: Option<TamRefCommune>,
    pub tam_ref_pays: Option<TamRefPays>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampAdherentParticipant {
    pub id: u64,
    pub date_debut_presence: Option<DateTime<Utc>>,
    pub date_fin_presence: Option<DateTime<Utc>>,
	pub adherent: AdherentInfo,
    pub chef_equipe: Option<bool>,
    pub coordonnees_parents: Option<String>,
    pub responsabilites: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampFichier {
    pub id: u64,
    pub categorie: String,
    pub sub_categorie: Option<String>,
    pub nom: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampDetail {
    pub id: u64,
    pub libelle: String,
    pub statut: CampStatut,
    pub date_debut: DateTime<Utc>,
    pub date_fin: DateTime<Utc>,
    pub type_camp: TypeCamp,
    pub camp_817: Option<Camp817>,
    pub camp_adherent_staffs: Option<Vec<CampAdherentStaff>>,
    pub camp_lieu_principal: Option<CampLieu>,
    pub camp_lieux: Option<Vec<CampLieu>>,
    pub camp_adherent_staffs_informations: Option<Vec<AdherentStaffInfo>>,
    pub camp_adherent_participants: Option<Vec<CampAdherentParticipant>>,
    pub camp_avis: Option<Vec<Avis>>,
    pub statut_declaration_tam: Option<DeclarationTam>,
    pub statut_depart_camp: Option<String>
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CampDetail817 {
    pub camp_817: Option<Camp817>,
    pub camp_adherent_staffs: Option<Vec<CampAdherentStaff>>,
    pub camp_lieu_principal: Option<CampLieu>,
    pub camp_lieux: Option<Vec<CampLieu>>,
    pub camp_adherent_staffs_informations: Option<Vec<AdherentStaffInfo>>,
}

#[derive(Debug, Deserialize)]
pub enum DeclarationTam {
    #[serde(rename(deserialize = "ND"))]
    Aucune,
    #[serde(rename(deserialize = "PD"))]
    Partielle,
    #[serde(rename(deserialize = "FD"))]
    Complete,
    #[serde(rename(deserialize = "UD"))]
    NonConfirmee,
    #[serde(rename(deserialize = "ID"))]
    Insuffisante,
    #[serde(rename(deserialize = "VD"))]
    Validee,
    #[serde(rename(deserialize = "MD"))]
    Modifiee,
}

#[derive(Debug, Deserialize)]
pub enum CodeGroupeCamp {
    #[serde(rename(deserialize = "FARFADET"))]
    Farfadet,
    #[serde(rename(deserialize = "8-17"))]
    _8_17,
    #[serde(rename(deserialize = "COMPAGNON"))]
    Compagnon,
    #[serde(rename(deserialize = "AUDACE"))]
    Audace,
}


impl FromStr for DeclarationTam {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "ND" => Ok(DeclarationTam::Aucune),
            "PD" => Ok(DeclarationTam::Partielle),
            "FD" => Ok(DeclarationTam::Complete),
            "UD" => Ok(DeclarationTam::NonConfirmee),
            "ID" => Ok(DeclarationTam::Insuffisante),
            "VD" => Ok(DeclarationTam::Validee),
            "MD" => Ok(DeclarationTam::Modifiee),
            _ => Ok(DeclarationTam::Aucune)
        }
    }
}

#[derive(Debug, Clone)]
enum SortDirection {
    Ascending,
    Descending,
}

impl fmt::Display for SortDirection {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SortDirection::Ascending => write!(f, "asc"),
            SortDirection::Descending => write!(f, "desc")
        }

    }
}

impl Default for SortDirection {
    fn default() -> Self { SortDirection::Descending }
}

#[derive(Debug, Clone)]
enum SortBy {
    Id,
    Structure,
    Label,
    Date,
}

impl fmt::Display for SortBy {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SortBy::Id => write!(f, "id"),
            SortBy::Structure => write!(f, "libelleStructure"),
            SortBy::Label => write!(f, "libelle"),
            SortBy::Date => write!(f, "dateDebut"),
        }

    }
}

impl Default for SortBy {
    fn default() -> Self { SortBy::Id }
}

#[derive(Debug, Default, Clone)]
pub struct CampQuery {
    sort_by: SortBy,
    sort_direction: SortDirection,
    page: u32,
}

impl CampQuery {
    pub fn new() -> CampQuery {
        CampQuery {
            sort_by: SortBy::Id,
            sort_direction: SortDirection::Descending,
            page: 1
        }
    }

    pub fn create(self) -> String {
        String::from(format!("camps?sortBy={}&sortDirection={}&selectedPage={}", self.sort_by, self.sort_direction, self.page))
    }

    pub fn set_page(mut self, page: u32) -> CampQuery {
        self.page = page;
        self
    }
}

#[derive(Debug, Clone)]
pub enum StatusCamp {
    EnCours,
    Annule,
    Archive,
}

impl fmt::Display for StatusCamp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            StatusCamp::EnCours => write!(f, "1"),
            StatusCamp::Annule => write!(f, "2"),
            StatusCamp::Archive => write!(f, "3"),
        }

    }
}

impl Default for StatusCamp {
    fn default() -> Self { StatusCamp::EnCours }
}

#[derive(Debug, Clone)]
pub struct MultiCriteresQuery {
    date_debut: NaiveDate,
    date_fin: NaiveDate,
    status_camp: Vec<StatusCamp>,
    code_structure: Option<String>,
    structures_dependantes: bool,
    page: u32,
}

impl MultiCriteresQuery {
    pub fn new() -> MultiCriteresQuery {
        let today = chrono::Utc::now().date_naive();
        let date_debut : NaiveDate;
        let date_fin : NaiveDate;
        if today.month() >= 9 {
            date_debut = chrono::NaiveDate::from_ymd_opt(today.year() + 1, 7, 1).unwrap();
            date_fin = chrono::NaiveDate::from_ymd_opt(today.year() + 1, 8, 31).unwrap();
        } else {
            date_debut = chrono::NaiveDate::from_ymd_opt(today.year(), 7, 1).unwrap();
            date_fin = chrono::NaiveDate::from_ymd_opt(today.year(), 8, 31).unwrap();
        }

        MultiCriteresQuery {
            date_debut: date_debut,
            date_fin: date_fin,
            status_camp: Vec::new(),
            code_structure: Option::None,
            structures_dependantes: true,
            page: 1
        }
    }

    pub fn create(self) -> String {
        let status_query = if self.status_camp.len() > 0 {
            format!("&statutsCamp={}", self.status_camp.iter().map(|val| {
                format!("{}", val)
            }).collect::<Vec<String>>().join(","))
        } else {
            "".to_string()
        };

        String::from(format!("multi-criteres?dateDebut={}&dateFin={}&chercherDossiersParticipants=0&codeStructure={}&chercherStructuresDependates={}{}&selectedPage={}",
            self.date_debut.format("%Y-%m-%dT00:00:00.000Z"),
            self.date_fin.format("%Y-%m-%dT00:00:00.000Z"),
            self.code_structure.unwrap(),
            if self.structures_dependantes == true { "1" } else { "0" },
            status_query,
            self.page)
        )
    }

    pub fn set_page(mut self, page: u32) -> MultiCriteresQuery {
        self.page = page;
        self
    }

    pub fn add_status(mut self, status: StatusCamp) -> MultiCriteresQuery {
        self.status_camp.push (status);
        self
    }

    pub fn set_structure(mut self, code_structure: String, sous_structures: bool) -> MultiCriteresQuery {
        self.code_structure = Option::Some(code_structure);
        self.structures_dependantes = sous_structures;
        self
    }
}
