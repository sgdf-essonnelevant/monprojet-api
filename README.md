# monprojet-api

Rust API for [monprojet.sgdf.fr](https://monprojet.sgdf.fr)

## Example

This project contains two small examples of exporters that generates .ini files.

To try it, simply run:
```sh
cargo run --example=camps -- -i IDENTIFIANT_INTRANET -p MOT_DE_PASSE_INTRANET -s CODE_STRUCTURE
```

```sh
cargo run --example=projet-annee -- -i IDENTIFIANT_INTRANET -p MOT_DE_PASSE_INTRANET -s CODE_STRUCTURE
```

```sh
cargo run --example=archive-camps -- -i IDENTIFIANT_INTRANET -p MOT_DE_PASSE_INTRANET -s CODE_STRUCTURE --regenerate=true
```

## License

Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://apache.org/licenses/LICENSE-2.0)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall
be dual licensed as above, without any additional terms or conditions.

